# Security

In this document the security aspects of this project are discussed. To check if the program is secure this application is tested against the OWASP top 10 and the OWASP [Zed Attack Proxy](https://www.zaproxy.org/)(ZAP). The [current](https://owasp.org/www-project-top-ten/2017/) version of the OWASP top 10 is from 2017. The OWASP top 10 can be found [here](## OWASP TOP 10) and ZAP reports [here](## OWASPS ZAP Scans).

- [Security](#security)
  - [OWASP TOP 10](#owasp-top-10)
    - [A1:2017-Injection](#a12017-injection)
    - [A2:2017-Broken Authentication](#a22017-broken-authentication)
    - [A3:2017-Sensitive Data Exposure](#a32017-sensitive-data-exposure)
    - [A4:2017-XML External Entities (XXE)](#a42017-xml-external-entities-xxe)
    - [A5:2017-Broken Access Control](#a52017-broken-access-control)
    - [A6:2017-Security Misconfiguration](#a62017-security-misconfiguration)
    - [A7:2017-Cross-Site Scripting (XSS)](#a72017-cross-site-scripting-xss)
    - [A9:2017-Using Components with Known Vulnerabilities](#a92017-using-components-with-known-vulnerabilities)
    - [A10:2017-Insufficient Logging & Monitoring](#a102017-insufficient-logging--monitoring)
  - [OWASPS ZAP Scans](#owasps-zap-scans)
    - [Initial Scan](#initial-scan)
      - [Result](#result)
        - [CSP: Wildcard Directive](#csp-wildcard-directive)
        - [X-Frame-Options Header Not Set](#x-frame-options-header-not-set)
        - [Absence of Anti-CSRF Tokens](#absence-of-anti-csrf-tokens)
        - [Application Error Disclosure](#application-error-disclosure)
        - [Cookie Without SameSite Attribute](#cookie-without-samesite-attribute)
        - [Cookie Without Secure Flag](#cookie-without-secure-flag)
        - [Incomplete or No Cache-control and Pragma HTTP Header Set](#incomplete-or-no-cache-control-and-pragma-http-header-set)
        - [Information Disclosure - Debug Error Messages](#information-disclosure---debug-error-messages)
        - [X-Content-Type-Options Header Missing](#x-content-type-options-header-missing)
        - [CSP: X-Content-Security-Policy](#csp-x-content-security-policy)
        - [Information Disclosure - Suspicious Comments](#information-disclosure---suspicious-comments)
        - [Loosely Scoped Cookie](#loosely-scoped-cookie)
        - [Timestamp Disclosure - Unix](#timestamp-disclosure---unix)
    - [Final scan](#final-scan)

## OWASP TOP 10

### A1:2017-Injection

To prevent injections `OWASP` recommends using an `ORM` and parameterized queries. In this project `EF Core` is used as the `ORM` and queries are written with `LINQ`. `LINQ` utilizes the `EF Core` parameterized functionality to write direct `SQL` queries.

Other forms of injection are not relevant for this project.

### A2:2017-Broken Authentication

`OWASP` recommends using `ASP.net Core Identity framework`, a secure password policy and a cookie policy. In this project Authentication As A Service was required. For this IdentityServer4 is used. IdentityServer4 takes all miss configuration out of the hands of developers. During setup the `is4aspid` preset was chosen. With this preset IdentityServer4 uses `.net Core Identity framework`.

### A3:2017-Sensitive Data Exposure

For setting up this project it is suggested to run it behind a proxy. A proxy can take care of all TLS1.2+. All sensitive data is handled by `IdentityServer4`. These are usernames and password.

### A4:2017-XML External Entities (XXE)

The project does not use XML as data format. All data is send over with JSON using the `Newtonsoft.Json` package. Data is not serialized but send over https.

### A5:2017-Broken Access Control

Most access control is handled by `IdentityServer4` where only `annotations` are used to authorize someone.

### A6:2017-Security Misconfiguration

To prevent security Misconfiguration all `http` requests are redirected to `https` with the following command: `app.UseHttpsRedirection();`. Further there are no `POST` or `PUT` request within the application. The only `POST` and `PUT` request are handled by `IdentityServer4` for Authentication. Because of this anti-forgerytokens are not needed.

### A7:2017-Cross-Site Scripting (XSS)

Because all data used by the application is trusted `XSS` is not an issue. `XSS` could be prevented by sanitizing all users input. By using `Razor` in this application it automatically encodes all output. `Content Security Policy` header is also a good way to prevent pages accessing assets they should not. In the application I added the following `CSP`: `default-src 'self'; object-src 'none'; form-action 'self'; frame-ancestors 'none'; sandbox allow-forms allow-same-origin allow-scripts; base-uri 'self'`. If this fails than the `X-XSS-Protection` can be used, but `CSP` is prefered over.

### A9:2017-Using Components with Known Vulnerabilities

To mitigate this the project is running the latest .Net framework version and packages. To reduce the chance of a Vulnerability the number of packages is kept to a minimum.

### A10:2017-Insufficient Logging & Monitoring

Monitoring the application is important to understand what is going wrong and who is abusing the service. Besides errors all user activity is logged within `IdentityServer4` For monitoring it uses Application Insights server-side telemetry by

## OWASPS ZAP Scans

For every scan there is an overview of the risks. Then there is an explanation where the risk occurs and how to fix it. The goal is to fix as many as possible. If a fix is not present there will be an explanation.

### Initial Scan

The full report can be found [here](./scans/initial.html)

| Name | Risk Level | Number of Instances |
| --- | --- | --- |
| CSP: Wildcard Directive | Medium | 27 |
| X-Frame-Options Header Not Set | Medium | 11 |
| Absence of Anti-CSRF Tokens | Low | 5 |
| Application Error Disclosure | Low | 2 |
| Cookie Without SameSite Attribute | Low | 5 |
| Cookie Without Secure Flag | Low | 3 |
| Incomplete or No Cache-control and Pragma HTTP Header Set | Low | 42 |
| Information Disclosure - Debug Error Messages | Low | 1 |
| X-Content-Type-Options Header Missing | Low | 33 |
| CSP: X-Content-Security-Policy | Informational | 27 |
| Information Disclosure - Suspicious Comments | Informational | 7 |
| Loosely Scoped Cookie | Informational | 28 |
| Timestamp Disclosure - Unix | Informational | 11766 |

#### Result

In the above result you can see where problems or points of attention are.

##### CSP: Wildcard Directive

During the scan in came to attention that `form-action` was not defined in the `CSP` configuration of `IdentityServer4`. to mitigate the `CSP` config was changed to: `default-src 'self'; object-src 'none'; frame-ancestors 'none'; form-action: 'self'; sandbox allow-forms allow-same-origin allow-scripts; base-uri 'self';`

##### X-Frame-Options Header Not Set

The X-Frame-Options Header is used to prevent `ClickJacking`. For both `IdentityServer4` and the application this line was added:
`context.Response.Headers["X-Frame-Options"] = "SAMEORIGIN";`

##### Absence of Anti-CSRF Tokens

During the ZAP scan it detected that no Anti-CSRF tokens were used for signin forms. After inspection this `IdentityServer4` did provide an Anti-CSRF. Therefor this issue is a false warning.

##### Application Error Disclosure

During testing the application was running in a development environment. Therefor the error page was not triggered, but a error disclosure to assist in debugging problems.

##### Cookie Without SameSite Attribute

To add the SameSite attribute the following code was added. The BrowserDetection comes from [microsoft](https://devblogs.microsoft.com/aspnet/upcoming-samesite-cookie-changes-in-asp-net-and-asp-net-core/)

```csharp
// ConfigureServices
services.Configure<CookiePolicyOptions>(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.Strict;
    options.OnAppendCookie = context => CheckSameSite(context.Context, context.CookieOptions);
    options.OnDeleteCookie = context => CheckSameSite(context.Context, context.CookieOptions);
});

private void CheckSameSite(HttpContext httpContext, CookieOptions options)
{
    if (options.SameSite == SameSiteMode.None)
    {
        var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
        if (BrowserDetection.DisallowsSameSiteNone(userAgent))
        {
            options.SameSite = SameSiteMode.Unspecified;
        }
    }
}
```

##### Cookie Without Secure Flag

To fix this issue the follow code was added to `IdentityServer4`

```csharp
services.AddAntiforgery(options =>
{
    options.Cookie.Domain = "localhost";
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.SuppressXFrameOptionsHeader = false;
});
```

##### Incomplete or No Cache-control and Pragma HTTP Header Set

After a closed session it might be possible to access private information due to the browser cache. Thereby `IdentityServer4` is configured for `Cache-Control: no-cache, no-store`.

Zap is suggesting to add the `must-revalidate` options to `Cache-Control` and a `Pragma: no-cache` header. This is done by adding the following code to `.addMvc` and `UseStaticFiles`. Also those settings can then by applied to the application.

```csharp
options.CacheProfiles.Add("Default", new CacheProfile()
{
    Location = ResponseCacheLocation.None,
    NoStore = true
});

app.UseStaticFiles(new StaticFileOptions
{
    OnPrepareResponse = context =>
    {
        context.Context.Response.Headers["Pragma"] = "no-cache";
        var headers = context.Context.Response.GetTypedHeaders();
        headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
        {
            NoStore = true,
            NoCache = true,
            MustRevalidate = true,
        };
    }
});
```

##### Information Disclosure - Debug Error Messages

This issue is also related to not running in production. Once running in production there will be an error page displayed.

##### X-Content-Type-Options Header Missing

To fix this issue add the following line to both `UseStaticFiles` and `app.use`: `context.Response.Headers["X-Content-Type-Options"] = "nosniff";`

##### CSP: X-Content-Security-Policy

ZAP is unable to verify if the CSP configuration is good enough. The people behind `IdentityServer4` have put thought into this.

##### Information Disclosure - Suspicious Comments

JavaScript that was loaded either from a framework or own code contains comments. This is a warning to check if none of those comments contain something about a possible vulnerability that is not fixed yet.

##### Loosely Scoped Cookie

During testing I tried scoping cookies, but this resulted in a cookie mismatch by with the application stopped working. Something to consider adding in the future. This warning is `Informational` and theirby not critical enough to make the application insecure.

##### Timestamp Disclosure - Unix

If encryption methods use timestamps to secure data leaking the datetime of the server can compromise this method. The application does not use this type of encryption and it's save to ignore this message.

### Final scan

After implementing all above. the Zap scan results into the following. A significant reduction in risks. Both `Absence of Anti-CSRF Tokens` and `Application Error Disclosure` can be ignore. These are not correctly detected. The full report can be found [here](./scans/final.html).

| Name | Risk Level | Number of Instances |
| --- | --- | --- |
| Absence of Anti-CSRF Tokens | Low | 4 |
| Application Error Disclosure | Low | 2 |
| Cookie Without SameSite Attribute | Low | 4 |
| Incomplete or No Cache-control and Pragma HTTP Header Set | Low | 38 |
