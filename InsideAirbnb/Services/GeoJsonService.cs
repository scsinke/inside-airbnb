using System.Collections.Generic;
using System.Linq;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using InsideAirbnb.Models;
using InsideAirbnb.Repositories;

namespace InsideAirbnb.Services
{
    public interface IGeoJsonService
    {
        public FeatureCollection GetGeoJson(IEnumerable<Listing> listings, string neighbourhood);
    }
    
    public class GeoJsonService : IGeoJsonService
    {
        private readonly INeighbourhoodGeoRepository _neighbourhoodGeoRepository;
        
        public GeoJsonService(INeighbourhoodGeoRepository neighbourhoodGeoRepository)
        {
            _neighbourhoodGeoRepository = neighbourhoodGeoRepository;
        }
        
        public FeatureCollection GetGeoJson(IEnumerable<Listing> listings, string neighbourhood)
        {
            var features = listings
                .Select(l => new Feature(new Point(new Position(l.Latitude, l.Longitude)), l, "listings"))
                .AsParallel()
                .WithExecutionMode(ParallelExecutionMode.ForceParallelism)
                .ToList();
            
            var featureCollection = new FeatureCollection(features);
            
            if (string.IsNullOrEmpty(neighbourhood)) return featureCollection;

            var locations = _neighbourhoodGeoRepository
            .GetByNeighbourhood(neighbourhood)
            .Select(n => new Position(n.Latitude, n.Longitude, n.Altitude))
            .ToList();

            if (!locations.Any()) return featureCollection;
            
            var poly = new Polygon(new List<LineString>
            {
                new(locations)
            });

            var polyFeature = new Feature(poly, null, "neighbourhood");
            featureCollection.Features.Add(polyFeature);
            
            return featureCollection;
        }
    }
}