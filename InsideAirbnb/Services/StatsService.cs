using System;
using System.Collections.Generic;
using System.Linq;
using InsideAirbnb.Models;
using InsideAirbnb.ViewModels;

namespace InsideAirbnb.Services
{
    public interface IStatsService
    {
        public StatsViewModel GetStats(IEnumerable<Listing> listings, bool getCharts = false);
    }
    public class StatsService : IStatsService
    {
        private readonly IChartService _chartService;

        public StatsService(IChartService chartService)
        {
            _chartService = chartService;
        }

        public StatsViewModel GetStats(IEnumerable<Listing> listings, bool getCharts = false)
        {
            var l = listings.ToList();
            var viewModel = new StatsViewModel(
                GetRoomStats(l),
                GetActivityStats(l),
                GetAvailabilityStats(l),
                GetListingStats(l)
            );

            if (getCharts)
                viewModel.ChartsData = _chartService.GetChartsData(viewModel, l);

            return viewModel;
        }

        private RoomStats GetRoomStats(List<Listing> listings)
        {
            var totalEntireHomes = listings.Count(l => l.RoomType == "Entire");
            var entireHomePercentage = listings.Count > 0
                ? Math.Round((double) totalEntireHomes / listings.Count * 100, 1)
                : 0.0;
            var averagePrice = Math.Round(listings
                .Select(l => l.Price)
                .DefaultIfEmpty()
                .Average());

            var totalPrivateRooms = listings.Count(l => l.RoomType == "Private");
            var totalSharedRooms = listings.Count(l => l.RoomType == "Shared");

            return new RoomStats(
                entireHomePercentage,
                averagePrice, 
                totalEntireHomes, 
                totalPrivateRooms,
                totalSharedRooms);
        }

        private ActivityStats GetActivityStats(List<Listing> listings)
        {
            var numberOfAvailability = listings
                .Select(l => l.Availability365)
                .DefaultIfEmpty()
                .ToList();

            var numberOfReviews = listings
                .Select(l => l.NumberOfReviews)
                .DefaultIfEmpty()
                .ToList();
            
            var estimatedNightsPerYear = Math.Round(numberOfAvailability.Average());
            var reviewsPerMonth = Math.Round(numberOfReviews.Average(), 2);
            var totalReviews = numberOfReviews.Sum();
            
            var averagePricePerNight = Math.Round(listings
                .Select(l => l.Price)
                .DefaultIfEmpty()
                .Average());
            var estimatedOccupancy = Math.Round(numberOfAvailability.Average() / 365 * 100, 1);
            var estimatedIncomePerMonth = Math.Round(listings
                .Select(l => l.Availability365 * l.Price / 12)
                .DefaultIfEmpty()
                .Average());
            
            return new ActivityStats(
                estimatedNightsPerYear,
                reviewsPerMonth,
                totalReviews,
                averagePricePerNight,
                estimatedOccupancy,
                estimatedIncomePerMonth);
        }

        private AvailabilityStats GetAvailabilityStats(List<Listing> listings)
        {
            var highAvailabilityListings = listings.Count(l => l.Availability365 > 60);
            var lowAvailabilityListings = listings.Count(l => l.Availability365 <= 60);
            var highAvailabilityListingsPercentage = listings.Count > 0
                ? Math.Round((double) highAvailabilityListings / listings.Count * 100, 1)
                : 0.0;
            var availabilityDays = Math.Round(listings
                .Select(l => l.Availability365)
                .DefaultIfEmpty()
                .Average(), 1);
            return new AvailabilityStats(
                highAvailabilityListingsPercentage,
                highAvailabilityListings,
                lowAvailabilityListings,
                availabilityDays);
        }

        private ListingStats GetListingStats(List<Listing> listings)
        {
            var multiListings = listings.Count(l => l.CalculatedHostListingsCount > 1);
            var multiListingsPercentage = listings.Count > 0
                ? Math.Round((double) multiListings / listings.Count * 100, 1)
                : 0.0;
            var singleListings = listings.Count(l => l.CalculatedHostListingsCount <= 1);
            
            return new ListingStats(multiListingsPercentage, singleListings, multiListings);
        }
    }
}
