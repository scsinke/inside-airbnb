using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using InsideAirbnb.Extensions;
using InsideAirbnb.Models;
using InsideAirbnb.ViewModels;

namespace InsideAirbnb.Services
{
    public interface IChartService
    {
        public List<ChartData> GetChartsData(StatsViewModel viewModel, List<Listing> listings);
    }
    public class ChartService : IChartService
    {
        public List<ChartData> GetChartsData(StatsViewModel viewModel, List<Listing> listings)
        {
            return new List<ChartData>
            {
                GetRoomChart(viewModel.RoomStats),
                GetActivityChart(listings),
                GetAvailabilityCategoryChart(viewModel.AvailabilityStats),
                GetAvailability365Chart(listings),
                GetListingsPerHostChart(listings)
            };
        }
            

        private ChartData GetRoomChart(RoomStats roomStats) => new(
            ChartType.RoomChart,
            new List<string>
            {
                "Entire home/apt",
                "Private room",
                "Shared room"
            },
            "horizontalBar",
            new List<double>
            {
                roomStats.TotalEntireHomes,
                roomStats.TotalPrivateRooms,
                roomStats.TotalSharedRooms
            },
            new List<string> {"Red", "Green", "Blue"});

        private ChartData GetActivityChart(IEnumerable<Listing> listings)
        {
            var ranges = new[] {0, 100, 200, 300, 400, 500, 1000, 2000, 3000, 4000, 5000, 10000};
            var priceRange = listings
                .GroupBy(l => ranges.FirstOrDefault(r => r > l.Price))
                .Select(g => new {Price = g.Key, Total = g.Count()});

            var labels = new List<string>();
            var data = new List<double>();
            
            foreach (var range in priceRange)
            {
                labels.Add(range.Price.ToString(CultureInfo.InvariantCulture));
                data.Add(range.Total);
            }
            
            return new ChartData(
                ChartType.ActivityChart,
                labels,
                "bar",
                data,
                new List<string>());
        }

        private ChartData GetAvailabilityCategoryChart(AvailabilityStats availabilityStats) => new(
            ChartType.AvailabilityCategoryChart,
            new List<string> {"High", "Low"},
            "pie",
            new List<double> {availabilityStats.HighAvailabilityListings, availabilityStats.LowAvailabilityListings},
            new List<string> {"Red", "Blue"}
        );

        private ChartData GetAvailability365Chart(IEnumerable<Listing> listings)
        {
            var res = listings
                .Where(l => l.Availability365 > 0)
                .GroupBy(l => l.Availability365)
                .Select(g => new {g.Key, Total = g.Count()})
                .OrderBy(g => g.Key);

            var labels = new List<string>();
            var data = new List<double>();
            
            foreach (var g in res)
            {
                labels.Add(g.Key.ToString());
                data.Add(g.Total);
            }
        
            return new ChartData(
            ChartType.Availability365Chart,
            labels,
            "bar",
            data,
            new List<string>());
            
        }

        private ChartData GetListingsPerHostChart(IEnumerable<Listing> listings)
        {
            var res = listings
                .DistinctBy(l => l.HostId)
                .GroupBy(l => l.CalculatedHostListingsCount)
                .Select(g => new { g.Key, Total = g.Count()});

            var labels = new List<string>();
            var data = new List<double>();
            
            foreach (var g in res)
            {
                labels.Add(g.Key.ToString());
                data.Add(g.Total);
            }

            var chart = new ChartData(
                ChartType.ListingsChart,
                labels,
                "bar",
                data,
                new List<string>());

            return chart;
        }
    }
}