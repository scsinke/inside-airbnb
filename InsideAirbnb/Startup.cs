using System.IdentityModel.Tokens.Jwt;
using InsideAirbnb.Repositories;
using InsideAirbnb.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;

namespace InsideAirbnb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = _ => true;
            });

            services.AddResponseCompression(options => options.EnableForHttps = true);

            services.AddControllersWithViews(options =>
            {
                options.CacheProfiles.Add("Default", new CacheProfile
                {
                    Location = ResponseCacheLocation.Any,
                    NoStore = false
                });
            }).AddNewtonsoftJson();
            services.AddRazorPages();

            services.AddDbContext<InsideAirbnbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IListingsRepository, ListingsRepository>();
            services.AddTransient<INeighbourhoodRepository, NeighbourhoodRepository>();
            services.AddTransient<INeighbourhoodGeoRepository, NeighbourhoodGeoRepository>();
            services.AddTransient<IGeoJsonService, GeoJsonService>();
            services.AddTransient<IStatsService, StatsService>();
            services.AddTransient<IChartService, ChartService>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetConnectionString("RedisCache");
            });

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = "Cookies";
                    options.DefaultChallengeScheme = "oidc";
                })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options =>
                {
                    var conf = Configuration.GetSection("Oidc");
                    options.SaveTokens = true;
                    options.RequireHttpsMetadata = false;
                    options.ClientId = conf["ClientId"];
                    options.ClientSecret = conf["ClientSecret"];
                    options.Authority = conf["Authority"];
                    options.SignedOutRedirectUri = "https://localhost:5001/";
                    options.Scope.Add("roles");
                    options.ClaimActions.MapJsonKey("role", "role", "role");
                    options.TokenValidationParameters.RoleClaimType = "role";
                    options.SignedOutRedirectUri = "/Home";
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseResponseCompression();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    context.Context.Response.Headers["X-Content-Type-Options"] = "nosniff";
                    context.Context.Response.Headers["Pragma"] = "no-cache";
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new CacheControlHeaderValue
                    {
                        NoStore = true,
                        NoCache = true,
                        MustRevalidate = true,
                    };
                }
            });
            
            app.UseCookiePolicy();
            app.Use((context, next) =>
            {
                context.Response.Headers["X-Content-Type-Options"] = "nosniff";
                context.Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
                return next.Invoke();
            });
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
