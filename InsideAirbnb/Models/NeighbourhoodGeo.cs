using InsideAirbnb.Models.Base;

namespace InsideAirbnb.Models
{
    public class NeighbourhoodGeo : Entity
    {
        public double Longitude { get; init; }
        public double Latitude { get; init; }
        public double Altitude { get; init; }

        public string NeighbourhoodId { get; init; }
        public Neighbourhood Neighbourhood { get; set; }
    }
}