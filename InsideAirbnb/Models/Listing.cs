using System;
using System.ComponentModel.DataAnnotations.Schema;
using InsideAirbnb.Models.Base;

namespace InsideAirbnb.Models
{
    [Serializable]
    public class Listing : Entity
    {
        public string Name { get; init; }
        public int HostId { get; init; }
        public string HostName { get; init; }

        #region Location
        public string NeighbourhoodId { get; init; }
        // [ForeignKey("NeighbourhoodId")]
        public virtual Neighbourhood Neighbourhood { get; set; }
        
        public double Latitude { get; init; }
        public double Longitude { get; init; }
        public string RoomType { get; init; }
        #endregion
     
        [Column(TypeName = "smallmoney")]
        public decimal Price { get; init; }
        public int MinimumNights { get; init; }

        #region Review
        public int NumberOfReviews { get; init; }
        public DateTime? LastReview { get; init; }
        public double? ReviewsPerMonth { get; init; }
        #endregion
       
        public int CalculatedHostListingsCount { get; init; }
        public int Availability365 { get; init; }
    }
}