using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace InsideAirbnb.Models.Base
{
    [Serializable]
    public class NamedEntity
    {
        [Key]
        [JsonProperty("Name", Required = Required.Always)]
        public string Name { get; set; }
    }
}