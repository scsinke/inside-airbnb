using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace InsideAirbnb.Models.Base
{
    [Serializable]
    public class Entity
    {
        [Key]
        [JsonProperty("Id", Required = Required.Always)]
        public int Id { get; set; }
    }
}