using Microsoft.AspNetCore.Mvc;

namespace InsideAirbnb.POCO
{
    public class FilterQueryModel
    {
        [FromQuery(Name = "neighbourhood")] public string Neighbourhood { get; set; } = "";
        [FromQuery(Name = "entireHomes")] public bool IsEntireRoom { get; set; } = false;
        [FromQuery(Name = "recentBooked")] public bool IsRecentlyBooked { get; set; } = false;
        [FromQuery(Name = "highAvailability")] public bool IsHighAvailable { get; set; } = false;
        [FromQuery(Name = "multiListing")] public bool IsMultipleListed { get; set; } = false;
        [FromQuery(Name = "priceRange")] public int PriceRange { get; set; } = 0;
        [FromQuery(Name = "reviewRange")] public int ReviewRange { get; set; } = 0;
    }
}