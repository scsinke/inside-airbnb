using GeoJSON.Net.Feature;
using InsideAirbnb.ViewModels;

namespace InsideAirbnb.POCO
{
    public class UpdateDto
    {
        public UpdateDto(FeatureCollection collection, StatsViewModel stats)
        {
            Collection = collection;
            Stats = stats;
        }

        public FeatureCollection Collection { get; }
        public StatsViewModel Stats { get; }
    }
}