namespace InsideAirbnb.ViewModels
{
    public class ActivityStats
    {
        public ActivityStats(double estimatedNightsPerYear, double reviewsPerMonth, int totalReviews, decimal averagePricePerNight, double estimatedOccupancy, decimal estimatedIncomePerMonth)
        {
            EstimatedNightsPerYear = estimatedNightsPerYear;
            ReviewsPerMonth = reviewsPerMonth;
            TotalReviews = totalReviews;
            AveragePricePerNight = averagePricePerNight;
            EstimatedOccupancy = estimatedOccupancy;
            EstimatedIncomePerMonth = estimatedIncomePerMonth;
        }

        public double EstimatedNightsPerYear { get; }
        public double ReviewsPerMonth { get; }
        public int TotalReviews { get; }
        public decimal AveragePricePerNight { get; }
        public double EstimatedOccupancy { get; }
        public decimal EstimatedIncomePerMonth { get; }
    }
}