using System.Collections.Generic;

namespace InsideAirbnb.ViewModels
{
    public class StatsViewModel
    {
        public StatsViewModel(RoomStats roomStats, ActivityStats activityStats, AvailabilityStats availabilityStats, ListingStats listingStats, List<ChartData> chartsData = null)
        {
            RoomStats = roomStats;
            ActivityStats = activityStats;
            AvailabilityStats = availabilityStats;
            ListingStats = listingStats;
            ChartsData = chartsData;
        }

        public RoomStats RoomStats { get; }
        public ActivityStats ActivityStats { get; }
        public AvailabilityStats AvailabilityStats { get; }
        public ListingStats ListingStats { get; }
        public List<ChartData> ChartsData { get; set; } 
    }
}