namespace InsideAirbnb.ViewModels
{
    public class RoomStats
    {
        public RoomStats(double entireHomePercentage, decimal averagePrice, int totalEntireHomes, int totalPrivateRooms, int totalSharedRooms)
        {
            EntireHomePercentage = entireHomePercentage;
            AveragePrice = averagePrice;
            TotalEntireHomes = totalEntireHomes;
            TotalPrivateRooms = totalPrivateRooms;
            TotalSharedRooms = totalSharedRooms;
        }
        public double EntireHomePercentage { get; }
        public decimal AveragePrice { get; }
        public int TotalEntireHomes { get; }
        public int TotalPrivateRooms { get; }
        public int TotalSharedRooms { get; }
    }
}