namespace InsideAirbnb.ViewModels
{
    public class AvailabilityStats
    {
        public AvailabilityStats(double highAvailabilityListingsPercentage, int highAvailabilityListings, int lowAvailabilityListings, double averageAvailabilityDays)
        {
            HighAvailabilityListingsPercentage = highAvailabilityListingsPercentage;
            HighAvailabilityListings = highAvailabilityListings;
            LowAvailabilityListings = lowAvailabilityListings;
            AverageAvailabilityDays = averageAvailabilityDays;
        }

        public double HighAvailabilityListingsPercentage { get; }
        public int HighAvailabilityListings { get; }
        public int LowAvailabilityListings { get; }
        public double AverageAvailabilityDays { get; }
    }
}