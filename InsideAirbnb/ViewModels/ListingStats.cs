namespace InsideAirbnb.ViewModels
{
    public class ListingStats
    {
        public ListingStats(double multiListingsPercentage, int singleListings, int multiListings)
        {
            MultiListingsPercentage = multiListingsPercentage;
            SingleListings = singleListings;
            MultiListings = multiListings;
        }

        public double MultiListingsPercentage { get; }
        public int SingleListings { get; }
        public int MultiListings { get; }
    }
}