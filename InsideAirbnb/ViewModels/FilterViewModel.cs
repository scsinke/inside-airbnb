using System.Collections.Generic;
using InsideAirbnb.Models;
using InsideAirbnb.POCO;

namespace InsideAirbnb.ViewModels
{
    public class FilterViewModel
    {
        public IEnumerable<Neighbourhood> Neighbourhoods;
        public FilterQueryModel QueryModel;

        public FilterViewModel(IEnumerable<Neighbourhood> neighbourhoods, FilterQueryModel queryModel)
        {
            Neighbourhoods = neighbourhoods;
            QueryModel = queryModel;
        }
    }
}