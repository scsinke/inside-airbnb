namespace InsideAirbnb.ViewModels
{
    public class HomeViewModel
    {
        public readonly FilterViewModel FilterViewModel;
        public readonly StatsViewModel StatsViewModel;

        public HomeViewModel(FilterViewModel filterViewModel, StatsViewModel statsViewModel)
        {
            FilterViewModel = filterViewModel;
            StatsViewModel = statsViewModel;
        }
    }
}