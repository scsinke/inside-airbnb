using System.Collections.Generic;

namespace InsideAirbnb.ViewModels
{
    public static class ChartType
    {
        public static string RoomChart => "roomTypeChart";
        public static string ActivityChart = "activityChart";
        public static string AvailabilityCategoryChart = "availabilityCategoryChart";
        public static string Availability365Chart = "availability365Chart";
        public static string ListingsChart = "listingsPerHostChart";

    }
    public class ChartData
    {
        public ChartData(string name, List<string> labels, string type, List<double> data, List<string> colors)
        {
            Name = name;
            Labels = labels;
            Type = type;
            Data = data;
            Colors = colors;
        }

        public string Name { get; }
        
        public List<string> Labels { get; }
        public string Type { get; }
        public List<double> Data { get; }
        public List<string> Colors { get; }
    }
}