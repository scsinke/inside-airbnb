using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsideAirbnb.Controllers
{
    public class AccountController: Controller
    {
        public IActionResult Register() => Redirect("https://localhost:5003/Account/Register");

        [Authorize]
        public IActionResult Login()
        {
            return Redirect("/");
        }

        [Authorize]
        public IActionResult Logout()
        {
            return SignOut("oidc");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
        
    }
}