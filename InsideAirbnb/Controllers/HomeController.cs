﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using InsideAirbnb.POCO;
using InsideAirbnb.Repositories;
using InsideAirbnb.Services;
using InsideAirbnb.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace InsideAirbnb.Controllers
{
    public class HomeController : Controller
    {
        private readonly INeighbourhoodRepository _neighbourhoodRepository;
        private readonly IListingsRepository _listingsRepository;
        private readonly IGeoJsonService _geoJsonService;
        private readonly IStatsService _statsService;
        private readonly IDistributedCache _redisCache;

        public HomeController(
            INeighbourhoodRepository neighbourhoodRepository,
            IListingsRepository listingsRepository,
            IGeoJsonService geoJsonService, 
            IStatsService statsService,
            IDistributedCache redisCache)
        {
            _neighbourhoodRepository = neighbourhoodRepository;
            _geoJsonService = geoJsonService;
            _statsService = statsService;
            _listingsRepository = listingsRepository;
            _redisCache = redisCache;
        }

        [Produces("text/html")]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> Index([FromQuery] FilterQueryModel queryModel)
        {
            var key = $"{Request.Path}{Request.QueryString}?isAdmin={User.IsInRole("Admin")}";
            var cacheRes = await _redisCache.GetStringAsync(key);
            if (cacheRes != null)
            {
                var model = JsonConvert.DeserializeObject<HomeViewModel>(cacheRes);
                return View(model);
            }
            var listings = _listingsRepository.GetFilterListings(queryModel);
            var stats = _statsService.GetStats(listings);
            var neighbourhoods = _neighbourhoodRepository.GetAllData().ToList();

            var viewModel = new HomeViewModel(
                new FilterViewModel(neighbourhoods, queryModel),
                stats
            );

            await _redisCache.SetStringAsync(key, JsonConvert.SerializeObject(viewModel));
            return View(viewModel);
        }

        [Produces("application/json")]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<UpdateDto> Update([FromQuery] FilterQueryModel queryModel)
        {
            var key = $"{Request.Path}{Request.QueryString}?isAdmin={User.IsInRole("Admin")}";
            var cacheRes = await _redisCache.GetStringAsync(key);
            if (cacheRes != null)
            {
                Console.WriteLine("From cache");
                return JsonConvert.DeserializeObject<UpdateDto>(cacheRes);
            }
            //heavy
            var listings = _listingsRepository.GetFilterListings(queryModel).ToList();
            var geoTask = Task.Factory.StartNew(() => _geoJsonService.GetGeoJson(listings, queryModel.Neighbourhood));
            var statTask = Task.Factory.StartNew(() => _statsService.GetStats(listings, User.IsInRole("Admin")));
            
            await Task.WhenAll(geoTask, statTask);
            var update = new UpdateDto(geoTask.Result, statTask.Result);
            await _redisCache.SetStringAsync(key, JsonConvert.SerializeObject(update));
            return update;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
