const mapCenter = [4.8951679, 52.3702157];
const charts = new Map();
let map;

getUrl = () => {
    const url = new URL(window.location.origin + "/Home/Update")
    url.search = new URLSearchParams({
        "neighbourhood": document.querySelector("select[id='neighbourhood']").value,
        "entireHomes": document.querySelector("input[id='RoomtypeEntire']").checked,
        "recentBooked": document.querySelector("input[id='recentBooked']").checked,
        "highAvailability": document.querySelector("input[id='highAvailable']").checked,
        "multiListing": document.querySelector("input[id='multiListing']").checked,
        "priceRange": document.querySelector("input[id='priceRange']").value,
        "reviewRange": document.querySelector("input[id='reviewRange']").value,
    });
    return url;
}

initialRender = async () => {
    map = renderMap()
    const url = getUrl();
    const data = await fetchData(url);
    addDataToMap(data.collection);

    renderStats(data.stats)
    if (data.stats.chartsData != null) {
        Chart.platform.disableCSSInjection = true;
        data.stats.chartsData.forEach(chartData => {
            renderChart(chartData)
        })
    }
}

addDataToMap = (data) => {
    map.addSource('data', {
        type: 'geojson',
        data: data
    });

    map.addLayer({
        id: 'neighbourhood',
        type: 'fill',
        source: 'data',
        layout: {},
        paint: {
            'fill-color': '#00e5e5',
            'fill-outline-color': '#006262',
            'fill-opacity': 0.3
        },
        filter: ['==', '$type', 'Polygon']
    });

    map.addLayer({
        id: 'listings',
        type: 'circle',
        source: 'data',
        filter: ['==', '$type', 'Point'],
        paint: {
            'circle-radius': {
                'base': 1.75,
                'stops': [[12, 2], [22, 180]]
            },
            'circle-color': [
                'match',
                ['get', 'RoomType'],
                'Entire', '#ff6633',
                'Private', '#339900',
                'Shared', '#00ccff',
                '#ccc'
            ],
            'circle-opacity': 0.8,
        }
    });
}

updateRender = async () => {
    const data = await fetchData();
    map.getSource('data').setData(data.collection)
    renderStats(data.stats)
    if (data.stats.chartsData != null) {
        data.stats.chartsData.forEach(chartData => {
            updateChart(chartData)
        })
    }
}

renderStats = ({ roomStats, activityStats, availabilityStats, listingStats }) => {
    const { entireHomePercentage, averagePrice, totalEntireHomes, totalPrivateRooms, totalSharedRooms } = roomStats
    const { estimatedNightsPerYear, reviewsPerMonth, totalReviews, averagePricePerNight, estimatedOccupancy, estimatedIncomePerMonth } = activityStats
    const { highAvailabilityListingsPercentage, highAvailabilityListings, lowAvailabilityListings, averageAvailabilityDays } = availabilityStats
    const { multiListingsPercentage, singleListings, multiListings } = listingStats

    document.querySelector(".EntireHomePercentage").innerHTML = entireHomePercentage
    document.querySelector(".AveragePrice").innerHTML = averagePrice
    document.querySelector(".TotalEntireHomes").innerHTML = totalEntireHomes
    document.querySelector(".TotalPrivateRooms").innerHTML = totalPrivateRooms
    document.querySelector(".TotalSharedRooms").innerHTML = totalSharedRooms

    document.querySelector(".EstimatedNightsPerYear").innerHTML = estimatedNightsPerYear
    document.querySelector(".ReviewsPerMonth").innerHTML = reviewsPerMonth
    document.querySelector(".TotalReviews").innerHTML = totalReviews
    document.querySelector(".AveragePricePerNight").innerHTML = averagePricePerNight
    document.querySelector(".EstimatedOccupancy").innerHTML = estimatedOccupancy
    document.querySelector(".EstimatedIncomePerMonth").innerHTML = estimatedIncomePerMonth

    document.querySelector(".HighAvailabilityListingsPercentage").innerHTML = highAvailabilityListingsPercentage
    document.querySelector(".HighAvailabilityListings").innerHTML = highAvailabilityListings
    document.querySelector(".LowAvailabilityListings").innerHTML = lowAvailabilityListings
    document.querySelector(".AverageAvailabilityDays").innerHTML = averageAvailabilityDays

    document.querySelector(".MultiListingsPercentage").innerHTML = multiListingsPercentage
    document.querySelector(".SingleListings").innerHTML = singleListings
    document.querySelector(".MultiListings").innerHTML = multiListings
}

fetchData = async () => {
    try {
        const url = getUrl();
        const response = await fetch(url);
        return await response.json();
    }
    catch (e) {
        console.error(e);
    }
}

renderMap = () => {
    mapboxgl.accessToken = 'pk.eyJ1Ijoic3RlaW5jYXJzdGVuc2lua2UiLCJhIjoiY2trcG10ZmgzMTI0eDJ4cndxcmEwdnNvNCJ9.0E1UPG3M2xQpRDJ0jRc36g';
    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: mapCenter,
        zoom: 10,
        maxZoom: 15,
        maxBounds: [
            [4.743994, 52.283190],
            [5.043202, 52.438759]
        ],
        pitchWithRotate: false,
    });

    map.addControl(new mapboxgl.NavigationControl());

    map.on('load', () => {
        map.on('mouseenter', 'listings', () => {
            map.getCanvas().style.cursor = 'pointer';
        });

        map.on('mouseleave', 'listings', () => {
            map.getCanvas().style.cursor = '';
        })

        map.on('click', 'listings', (e) => {
            const coordinates = e.features[0].geometry.coordinates.slice();
            const { properties } = e.features[0];

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates)
                .setHTML(renderDescription(properties))
                .addTo(map);
        })
    });

    return map
}

renderDescription = (listingData) => {
    return `
        <div class="popup">
            <div class="popup-header">
                ${listingData.Name}
            </div>
            <div class="popup-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Host</td>
                        <td>${listingData.HostName}</td>
                    </tr>
                    <tr>
                        <td>RoomType</td>
                        <td>${listingData.RoomType}</td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>${listingData.Price}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `
}

renderChart = (chartData) => {
    const chart = document.querySelector(`#${chartData.name}`).getContext('2d');
    const chartJs = new Chart(chart, {
        type: chartData.type,
        data: {
            labels: chartData.labels,
            datasets: [{
                data: chartData.data,
                backgroundColor: chartData.colors
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }], 
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    })
    charts.set(chartData.name, chartJs);
}

updateChart = (chartData) => {
    const chart = charts.get(chartData.name);
    if (chart !== undefined){
        chart.data.labels = chartData.labels;
        chart.data.datasets = [{
            data: chartData.data,
            backgroundColor: chartData.colors
        }];
        chart.update();
    }
}
