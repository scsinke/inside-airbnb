using System;
using System.Collections.Generic;
using System.Linq;
using InsideAirbnb.Models;
using InsideAirbnb.POCO;
using Microsoft.EntityFrameworkCore;

namespace InsideAirbnb.Repositories
{
    public interface IListingsRepository : IRepository<Listing>
    {
        public IEnumerable<Listing> GetFilterListings(FilterQueryModel queryModel);
    }
    public class ListingsRepository : Repository<Listing>, IListingsRepository
    {
        public ListingsRepository(InsideAirbnbContext context) : base(context) { }
        public IEnumerable<Listing> GetFilterListings(FilterQueryModel queryModel)
        {
            var listings = Context.Listings.AsNoTracking();

            if (queryModel.PriceRange > 0)
                listings = listings.Where(l => l.Price >= queryModel.PriceRange);

            if(queryModel.ReviewRange > 0)
                listings = listings.Where(l => l.NumberOfReviews >= queryModel.ReviewRange);

            if (!string.IsNullOrEmpty(queryModel.Neighbourhood))
                listings = listings.Where(l => l.NeighbourhoodId == queryModel.Neighbourhood);

            if (queryModel.IsEntireRoom)
                listings = listings.Where(l => l.RoomType == "Entire");

            if (queryModel.IsRecentlyBooked)
                listings = listings.Where(l => l.LastReview > DateTime.Now.AddMonths(-6));

            if (queryModel.IsHighAvailable)
                listings = listings.Where(l => l.Availability365 > 60);

            if (queryModel.IsMultipleListed)
                listings = listings.Where(l => l.CalculatedHostListingsCount > 1);

            return listings;
        }
    }
}