using InsideAirbnb.Models;

namespace InsideAirbnb.Repositories
{
    public interface INeighbourhoodRepository : IRepository<Neighbourhood> { }
    
    public class NeighbourhoodRepository : Repository<Neighbourhood>, INeighbourhoodRepository
    {
        public NeighbourhoodRepository(InsideAirbnbContext context) : base(context) { }
    }
}