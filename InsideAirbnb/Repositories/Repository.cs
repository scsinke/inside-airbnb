using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace InsideAirbnb.Repositories
{
    public interface IRepository<T>
    {
        public IEnumerable<T> GetAllData();
    }
    
    public class Repository<T> : IRepository<T> where T : class
    {
        public readonly InsideAirbnbContext Context;

        public Repository(InsideAirbnbContext context)
        {
            Context = context;
        }

        public IEnumerable<T> GetAllData() => Context
            .Set<T>()
            .AsNoTracking();
    }
}