using System.Linq;
using InsideAirbnb.Models;
using Microsoft.EntityFrameworkCore;

namespace InsideAirbnb.Repositories
{
    public interface INeighbourhoodGeoRepository : IRepository<NeighbourhoodGeo>
    {
        public IQueryable<NeighbourhoodGeo> GetByNeighbourhood(string neighbourhood);
    }

    public class NeighbourhoodGeoRepository : Repository<NeighbourhoodGeo>, INeighbourhoodGeoRepository
    {
        public NeighbourhoodGeoRepository(InsideAirbnbContext context) : base(context) { }

        public IQueryable<NeighbourhoodGeo> GetByNeighbourhood(string neighbourhood) =>
            Context
                .Set<NeighbourhoodGeo>()
                .AsNoTracking()
                .Where(n => n.NeighbourhoodId == neighbourhood);

        // .ToList();
    }
}