using InsideAirbnb.Models;
using Microsoft.EntityFrameworkCore;

namespace InsideAirbnb.Repositories
{
    
    public class InsideAirbnbContext: DbContext
    {
        public InsideAirbnbContext(DbContextOptions<InsideAirbnbContext> options) : base(options) {}
        
        public virtual DbSet<Listing> Listings { get; set; }
        public virtual DbSet<Neighbourhood> Neighbourhoods { get; set; }
        public virtual DbSet<NeighbourhoodGeo> NeighbourhoodGeos { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     optionsBuilder.LogTo(Console.WriteLine);
        // }
    }
}