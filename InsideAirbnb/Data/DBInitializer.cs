using System;
using System.Globalization;
using System.Linq;
using InsideAirbnb.Models;
using InsideAirbnb.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;

namespace InsideAirbnb.Data
{
    public static class DbInitializer
    {
        public static void Initialize(InsideAirbnbContext context)
        {
            context.Database.EnsureCreated();

            if (!context.Neighbourhoods.Any())
            {
                CreateNeighbourhoods(context);
            }
            
            if (!context.Listings.Any())
            {
                CreateListings(context);
            }
        }

        private static void CreateNeighbourhoods(InsideAirbnbContext context)
        {
            var parser = GetParser("neighbourhoods.csv");

            while (!parser.EndOfData)
            {
                try
                {
                    var neighbourhood =  GetNeighbourhoodFromCsv(parser.ReadFields());
                    context.Neighbourhoods.Add(neighbourhood);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            context.SaveChanges();
        }

        private static void CreateListings(InsideAirbnbContext context)
        {
            var transaction = context.Database.BeginTransaction();
            context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Listings ON;");

            var parser = GetParser("listings.csv");

            while (!parser.EndOfData)
            {
                try
                {
                    var listing = GetListingFromCsv(parser.ReadFields());
                    context.Add(listing);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            
            context.SaveChanges();
            context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Listings OFF;");
            transaction.Commit();
        }

        private static TextFieldParser GetParser(string file)
        {
            var parser = new TextFieldParser(file)
            {
                CommentTokens = new[] {"#"},
                HasFieldsEnclosedInQuotes = true,
                Delimiters = new[] {","}
            };
            parser.ReadLine();
            return parser;
        }

        private static Neighbourhood GetNeighbourhoodFromCsv(string[] values) =>
            new()
            {
                Name = values[1]
            };

        private static Listing GetListingFromCsv(string[] values)
        {
            var id = Convert.ToInt32(values[0]);
            var hostId = Convert.ToInt32(values[2]);
            double latitude = Convert.ToDouble(values[6]);
            double longitude = Convert.ToDouble(values[7]);
            var myNfi = new NumberFormatInfo
            {
                NegativeSign = "-",
                CurrencyDecimalSeparator = ".",
                CurrencyGroupSeparator = ",",
                CurrencySymbol = "$"
            };
            var price = Decimal.Parse(values[9], NumberStyles.Currency, myNfi);
            int minimumNights = Convert.ToInt32(values[10]);
            int numberOfReviews = Convert.ToInt32(values[11]);
            DateTime? lastReview = string.IsNullOrWhiteSpace(values[12]) ? null :Convert.ToDateTime(values[12]);
            double? reviewsPerMonth = string.IsNullOrWhiteSpace(values[13]) ? null :Convert.ToDouble(values[13]);
            int calculatedHostListingsCount = Convert.ToInt32(values[14]);
            int availability365 = Convert.ToInt32(values[15]);
            
            return new Listing
            {
                Id = id,
                Name = values[1],
                HostId = hostId,
                HostName = values[3],
                NeighbourhoodId = values[5],
                Latitude = latitude,
                Longitude = longitude,
                RoomType = values[8],
                Price = price,
                MinimumNights = minimumNights,
                NumberOfReviews = numberOfReviews,
                LastReview = lastReview,
                ReviewsPerMonth = reviewsPerMonth,
                CalculatedHostListingsCount = calculatedHostListingsCount,
                Availability365 = availability365
            };
        }
    }
}