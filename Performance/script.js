import http from 'k6/http';

export const options = {
  vus: 1,
  duration: '2m'
}

export default function() {
  http.get(`https://${__ENV.MY_HOSTNAME}`);
}
