# Performance

This application should be highly-scalable. To achieve this we are going to measure different things. The application endpoints will be measured seperately. So static options are measure seperate from and api call for data from the database. For api call testing [k6](https://k6.io/) will be used. To measure the performance a Smoke test it used. If we can validate that the `http_req_waiting` goes down we are on the right track. Before we can increase the performance we need to identify what could improve it.

To run the test run the following command with `k6` installed.

```bash
k6 run -e MY_HOSTNAME='localhost:5001' script.js && k6 run -e MY_HOSTNAME='localhost:5001?neighbourhood=&entireHomes=true&recentBooked=true&highAvailability=true&multiListing=true&priceRange=145&reviewRange=0' script.js && k6 run -e MY_HOSTNAME='localhost:5001/Home/Update' script.js && k6 run -e MY_HOSTNAME='localhost:5001/Home/Update?neighbourhood=&entireHomes=true&recentBooked=true&highAvailability=true&multiListing=true&priceRange=145&reviewRange=0' script.js
```

## Possible improvements

- PLINQ
- Redis cache
- Compression
  - Brotli
  - GZIP

## Baseline

The baseline is already using the `.AsNoTracking()` method.

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/|193.9ms|116.76ms|185.22ms|1.16s|247.02ms|273.45ms|
|/?query|13.83ms|10.3ms|13.11ms|112.55ms|17.03ms|18.67ms|
|/Home/Update|544.6ms|417.75ms|531.19ms|794.01ms|653.64ms|693.59ms|
|/Home/Update?query|17.41ms|12.15ms|16.24ms|97.56ms|21.91ms|24.68ms|

## Fixes

### parallel geo and stats

By trying to run the creation of the feature collection and statsviewmodel in parallel I'm trying to reduce the amount it takes for the respanse. It's clear there is a small improvement. But not the improvement we should be looking at.

```csharp
var geoTask = Task.Factory.StartNew(() => _geoJsonService.GetGeoJson(listings, queryModel.Neighbourhood));
var statTask = Task.Factory.StartNew(() => _statsService.GetStats(listings, true));           
await Task.WhenAll(geoTask, statTask);
```

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|531.26ms|416.51ms|515.09ms|1.6s|612.8ms|634.86ms|
|/Home/Update?query|16.06ms|11.75ms|15.3ms|84.82ms|19.6ms|21.34ms|

### listings as parallel

Trying to run the listing request in parallel could possibly reduce request time. But as you can see based on the results it increases again. So this change dit not help.

```csharp
listings
  . AsParallel()
  .WithExecutionMode(ParallelExecutionMode.ForceParallelism);
```

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|566.5ms|438.66ms|542.07ms|1.66s|657.99ms|681.52ms|
|/Home/Update?query|17.51ms|12.32ms|16.3ms|1.01s|21.58ms|23.9ms|

### Parallel feature creation

```csharp
listings
.Select(l => new Feature(new Point(new Position(l.Latitude, l.Longitude)), l, "listings"))
.AsParallel()
.WithExecutionMode(ParallelExecutionMode.ForceParallelism);
```

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|523ms|410.72ms|486.29ms|1.24s|664.01ms|770.3ms|
|/Home/Update?query|16.98ms|12.39ms|15.82ms|303.21ms|20.56ms|23.01ms|

### Checking if person asks for neighbourhood

This should reduce the amount of times a database call is made. A database call adds latency

```csharp
if (string.IsNullOrEmpty(neighbourhood)) return featureCollection;

var locations = _neighbourhoodGeoRepository
.GetByNeighbourhood(neighbourhood)
.Select(n => new Position(n.Latitude, n.Longitude, n.Altitude))
.ToList();
```

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|491.05ms|386ms|462.68ms|1.21s|557.49ms|735.81ms|
|/Home/Update?query|13.65ms|10.48ms|12.92ms|169.68ms|15.98ms|17.74ms|

### Features assignment instead of addRange

First the features where added by `.addRange`, now it's an assignment. This reduces a loop.

Before:

```csharp
var featureCollection = new FeatureCollection();

var features = listings
.Select(l => new Feature(new Point(new Position(l.Latitude, l.Longitude)), l, "listings"))
.AsParallel()
.WithExecutionMode(ParallelExecutionMode.ForceParallelism)

featureCollection.addRange(featureCollection);
```

After:

```csharp
var features = listings
.Select(l => new Feature(new Point(new Position(l.Latitude, l.Longitude)), l, "listings"))
.AsParallel()
.WithExecutionMode(ParallelExecutionMode.ForceParallelism)
.ToList();
            
var featureCollection = new FeatureCollection(features);
```

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|480.04ms|397.64ms|456.95ms|801.26ms|582.28ms|665.99ms|
|/Home/Update?query|13.28ms|10.48ms|12.77ms|249.09ms|15.38ms|16.5ms|

### Adding Compression

This does not result in a better average `http_req_waiting`. But does speed up the web page. By using `Lighthouse` the performance score [increased](./scans/lighthouseCompression.html).

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|482.8ms|404.45ms|462.93ms|1.46s|555.63ms|572.35ms|
|/Home/Update?query|12.68ms|10.54ms|12.3ms|1.02s|13.96ms|14.67ms|

### Adding Redis cache

By implementing redis cache the results with smaller query significantly reduce. But for large objects the result is getting slower. This is the result of redis not being very good at large object caching. Besides the larger object now the application needs to deserialize the object. A possible solution for this is to move to a binary format before storing in the cache. 

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/Home/Update|542.4ms|352.35ms|430.61ms|1.71s|882.06ms|915.22ms|
|/Home/Update?query|9.48ms|3.64ms|9.02ms|1s|11.83ms|13.11ms|

### Improving JS request

By letting the map render before the data arrives the `Largest Contentful Paint` metric in `Lighthouse` improves. See [lighthouse](./scans/lighthouseLargeContentfulPaint.html) result and [commit](2b8c0a8462000f03e343a02c66cba8eaab6d7631) change.

## Conclusion

As a result of all the changes we see a good improvement across the board. The only exception here is the `/Home/Update` with an increase. This due to the large object size as stated [here](### Adding Redis cache).

|url|avg|min|med|max|p(90)|p(95)|
| --- | --- | --- | --- | --- | --- | --- |
|/                  |2.57ms (-19.33ms)  |1.56ms (-11.2ms)     |2.44ms (-182.78ms)   |91.3ms (-1068.7ms) |3.23ms (-243.79ms) |3.5ms (-273.10ms)|
|/?query            |2.81ms (-11.02ms)  |1.55ms (-875ms)      |2.56ms (-10.55ms)    |130.28ms (+17.73ms)|3.68ms (-13.35ms)  |4.36ms (-14.31ms)|
|/Home/Update       |979.5ms (+434.9ms) |843.44ms (+425.69ms) |933.2ms (+402.01ms)  |1.48s (+685.99ms)  |1.12s (+466.36ms)  |1.15s (+456.41ms)|
|/Home/Update?query |15.87ms (-1.54ms)  |6.54ms (-5.61ms)     |15.75ms (-0.49ms)    |188.2ms (+90.64ms) |18.57ms (-3.34ms)  |19.75ms (-4.93ms)|

Besides the request improvement are the Javascript improvements also notible.

|Metric|Before|After|
| --- | --- | --- |
|First Contentful Paint|1.8s|0.8|
|Speed Index|2.1s|1.4s|
|Largest Contentful Paint|15.8s|0.8s|
|Time to Interactive|2.9s|1.8s|
|Total Blocking Time|420ms|450ms|

With the in mind that the redis cache can be optimized I feel confident that this is an performant application.
