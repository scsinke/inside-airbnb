# Inside Airbnb

- [Inside Airbnb](#inside-airbnb)
  - [About The Project](#about-the-project)
    - [Build With](#build-with)
    - [Getting Started](#getting-started)
      - [Prerequisites](#prerequisites)
      - [Installation](#installation)
      - [Configuration](#configuration)
  - [Documentation](#documentation)
    - [Architecture](#architecture)
    - [Dependencies](#dependencies)
    - [Technical requirements](#technical-requirements)
    - [Use cases implemented](#use-cases-implemented)
  - [Acknowledgements](#acknowledgements)

## About The Project

![InsideAirbnb Screen shot](./Screenshot.png)
View Airbnb listings and stats of Amsterdam as part of a school project.

### Build With

- [.NET5](https://docs.microsoft.com/en-us/dotnet/core/dotnet-five)
- [Chart.js](https://www.chartjs.org/)
- [Mapbox](https://www.mapbox.com/)
- [Redis](https://redis.io/)
- [IdentityServer4](https://identityserver.io/)
- [Bootstrap](https://getbootstrap.com/)

### Getting Started

To get a local copy up and running follow these simple steps

#### Prerequisites

Install .Net

```sh
brew install --cask dotnet-sdk
```

Start a SQL server

```sh
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest
```

#### Installation

- Clone the repo

  ```sh
  git clone https://gitlab.com/scsinke/inside-airbnb
  ```

- Install Nuget packages

  ```sh
  dotnet restore
  ```

#### Configuration

- Inside `appsettings.json` add your own database and IdentityServer4 credentials.
- Modify mapbox access token in `home.js`

## Documentation

### Architecture

![Architecture](Architecture.png)

### Dependencies

| Dependency | Reason for inclusion |
| GeoJSON.Net | Handeling GeoJSON data |
| Microsoft.AspNetCore.Authentication.OpenIdConnect | Authentication and Authorization |
| Microsoft.AspNetCore.Mvc.NewtonsoftJson | JSON files |
| Microsoft.EntityFrameworkCore | Used for data models |
| Microsoft.EntityFrameworkCore.Design | Used for migrations |
| Microsoft.EntityFrameworkCore.SqlServer | Connect to database |
| Microsoft.Extensions.Caching.StackExchangeRedis | Used for connecting to Redis |
| Newtonsoft.Json | Used by GeoJSON.Net to render GeoJSON to JSON |
| Chart.js | Rendering Chart in DOM |
| Mapbox-gl-js | Rendering Map in DOM |

### Technical requirements

Security report can be found [here](./Security/README.md)
Performance report can be found [here](./Performance/README.md)

- [x] Developed with latest .NET Core version
- [x] Uses ASP.Net MVC
- [x] Uses MSSQL server
- [x] Check on OWASP TOP 10
- [x] High performance capable
- [x] Authentication As A Service by using `IdentityServer4`
- [x] Caching service with Redis

### Use cases implemented

- [x] Register and login
- [x] Filter based on price
- [x] Filter based on neighbourhood
- [x] Filter based on reviews
- [x] Location of listings visible on map
- [x] Map is clickable
- [x] layout as [InsideAirbnb](http://insideairbnb.com/amsterdam/)
- [x] Item details
- [x] Roles added to registered users
- [x] Results are shown in charts for Admin users
- [x] Trends are visible

## Acknowledgements

Project made in commission by [Hogeschool van Arnhem en Nijmegen](https://www.han.nl/) for the Web Applications course.
