﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System.Collections.Generic;
using IdentityServer4.Models;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
                   new IdentityResource[]
                   {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new("roles", new []{"role"})
                   };

        public static IEnumerable<ApiResource> GetApis =>
            new ApiResource[]
            {
                new("api1", "My API #1")
            };

        public static IEnumerable<Client> Clients =>
            new[]
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "Client Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "api1" }
                },
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    
                    AllowedGrantTypes = GrantTypes.Implicit,
                    ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256())},
                    
                    RedirectUris = { "https://localhost:5001/signin-oidc" },
                    FrontChannelLogoutUri = "https://localhost:5001/signout-oidc",
                    PostLogoutRedirectUris = { "https://localhost:5001/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "api1", "roles" }         

                },
                new Client
                {
                    ClientId = "spa",
                    ClientName = "SPA Client",
                    ClientUri = "http://identityserver.io",

                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =
                    {
                        "http://localhost:5002/index.html",
                        "http://localhost:5002/callback.html",
                        "http://localhost:5002/silent.html",
                        "http://localhost:5002/popup.html",
                    },

                    PostLogoutRedirectUris = { "http://localhost:5002/index.html" },
                    AllowedCorsOrigins = { "http://localhost:5002" },

                    AllowedScopes = { "openid", "profile", "api1" }
                }

                // // m2m client credentials flow client
                // new Client
                // {
                //     ClientId = "m2m.client",
                //     ClientName = "Client Credentials Client",
                //
                //     AllowedGrantTypes = GrantTypes.Implicit,
                //     ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },
                //
                //     AllowedScopes = { "scope1" }
                // },
                //
                // // interactive client using code flow + pkce
                // new Client
                // {
                //     ClientId = "interactive",
                //     ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },
                //
                //     AllowedGrantTypes = GrantTypes.Code,
                //
                //     RedirectUris = { "https://localhost:44300/signin-oidc" },
                //     FrontChannelLogoutUri = "https://localhost:44300/signout-oidc",
                //     PostLogoutRedirectUris = { "https://localhost:44300/signout-callback-oidc" },
                //
                //     AllowOfflineAccess = true,
                //     AllowedScopes = { "openid", "profile", "scope2" }
                // },
            };
    }
}